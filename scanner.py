import os
import json
import urllib2
import re

class Scanner:

    result = []
    driver = None
    movies_directory = 'Movies'
    omdb = 'http://www.omdbapi.com/'
    movies = []

    def __init__(self):
        self.get_file_names()
        self.process_movies()
        self.save_json()

    def save_json(self):
        with open('result.json', 'w') as outfile:
            json.dump(self.result, outfile, ensure_ascii=False)

    def get_file_names(self):
        for dir_name, sub_dir_list, file_list in os.walk(self.movies_directory):
            for file in file_list:
                if file.lower().endswith(('.video', '.files', '.extentions')):
                    movie_info = {}
                    movie_info['file'] = file.lower().split('.')[0]
                    movie_info['path'] = dir_name + '/' + file
                    self.movies.append(movie_info)

    def process_movies(self):
        for movie in self.movies:
            self.result.append(self.get_movie_info(movie))

    def get_movie_info(self, movie):
        match = re.search('\d{4}', movie['file'])
        year = match.group(0).strip().replace(' ', '%20') if match else ''
        title = movie['file'].replace(year, '').strip().replace(' ', '%20') if match else movie['file'].strip().replace(' ', '%20')
        request = urllib2.urlopen(self.omdb+'?t='+title+'&y='+year+'&r=json').read()

        movie_object = json.loads(request, object_hook=self.ascii_encode_dict)
        movie_object['File'] = movie['file']
        movie_object['filePath'] = movie['path']

        return  movie_object

    def ascii_encode_dict(self, data):
        ascii_encode = lambda x: x.encode("utf-8")
        return dict(map(ascii_encode, pair) for pair in data.items())

Scanner()
